CakePhp 構築
========================
***

## CakePhp 構築

#### 1. xampp インストール

#### 2. composerをinternetでインストールファイルをダウンロードしてインストール
> composerインストール確認はcmdでcomposerと入力してみる

#### 2-1. フォルダにcomposer install実行
> composer.lockに定義されたpackageをインストール

#### 2-2. composer.jsonを修正した場合はcomposer update実行
> composer.jsonの内容をcomposer.lockにアップデート

#### 3. sourceTreeにリポジトリ追加
> xampp/htdocsにソースをダウンロード

#### 4. 他のプロジェクトのvenderフォルダをコピーして貼り付けする

#### 5. .env.defaultファイルを .envファイルに変更
> DB設定確認

#### 6. hostファイルに設定(APIのため)
__位置__

    C:\Windows\System32\drivers\etc
__追加__

    192.168.10.10 welby-share-api.localhost.jp
    192.168.10.10 welby-userapi.localhost.jp
    192.168.10.10 welby-userapi-web.localhost.jp
    192.168.10.10 welby-sz-support-app.localhost.jp

#### 新しいプロジェクト生成
> composer create-project --prefer-dist cakephp/app project_name



<br>
##### ※分からない
>C:\xampp\php\composer.phar install

<br><br>
## CakePhp 基本設定
#### 1. console /binに
- 全部生成
    - app
~~~
    cake bake all AppAdmins
    cake bake all MMedicines
    cake bake all MSelfcheckQuestions
    cake bake all Profiles
~~~
    - doctor
~~~
    cake bake all　doc_admins
    cake bake all　doctors
    cake bake all　ad_logs
    cake bake all　ads
    cake bake all　merchants
~~~
- model生成
    - app
~~~
    cake bake model MPrefs
    cake bake model SelfcheckResults
    cake bake model SleepLogs
    cake bake model MedicineLogs
    cake bake model MedicineRegisters
    cake bake model MedicineLefts
    cake bake model MedicineMemos
    cake bake model TimeSettings
    cake bake model Appointments
    cake bake model doctors
    cake bake model charge_patients
~~~
    - doctor
~~~
    cake bake model MPrefs
    cake bake model MMedicines
    cake bake model MSelfcheckQuestions
    cake bake model Profiles
    cake bake model Appointments
    cake bake model SelfcheckResults
    cake bake model SleepLogs
    cake bake model MedicineLogs
    cake bake model MedicineLefts
    cake bake model MedicineMemos
    cake bake model MedicineRegisters

    cake bake model action_logs
    cake bake model actions
    cake bake model ad_summaries
    cake bake model charge_patients
~~~



#### 2. 自動的に作られたので、tableの関係をよく見なければならない
　　たとえば、AppAdminsはadminと関係がない


## < PhantomJs　インストール >
1. composer.jsonに追加
```json
  "scripts": {
      "post-install-cmd": [
          "PhantomInstaller\\Installer::installPhantomJS"
      ],
      "post-update-cmd": [
          "PhantomInstaller\\Installer::installPhantomJS"
      ]
  }
  "config": {
        "bin-dir": "bin"
  }
```

2. commandで root フォルダに下記を入力  
> $ composer require "jonnyw/php-phantomjs:4.* "

3. Controllerで基本テスト


※ set できない場合はControllerで下記を入力
> $client->getEngine()->setPath(ROOT . DS . 'bin' . DS . 'phantomjs.exe');


<br><br>
## < log 出力 >
#### ※CakePhpのDebugKitに出力する場合
```php
use Cake\Log\Log;
Log::write('debug', print_r($this->name, true));
```

#### ※powsershellに出力する場合
##### (CakephpProject/logsに出力)
```php
$this->log($this, LOG_DEBUG);
```

~~~
cd 'C:\xampp\htdocs\welby-sz-support-app\logs'
Get-Content debug.log -Wait -Tail 500 -Encoding UTF8
~~~

##### ※(php_error_logに出力)
```php
error_log(print_r($data, true));
```

<br><br>
## < sass インストール(cmdで) >
#### 1. rubyインストール
    ruby install
    http://rubyinstaller.org/downloads/

#### 1-1. 確認
    ruby -v

#### 2. cssがあるフォルダで
    gem install sass

#### 3.
    gem update sass

#### 4.
    gem install compass

#### 5.
    compass create --sass-dir "sass" --css-dir "css" --javascripts-dir "js" --images-dir "img"

#### ※ scssに変更するcssファイルがある場合
    sass-convert --from css --to scss css/style.css sass/style.scss


<br><br>
## < sass利用(cmdで) >
    cd c:/xampp/htdocs/welby-sz-support-app/webroot
    compass watch
　　__=>compile 完了__


<br><br>
## < DB接続 >
#### 1. aws接続
#### 2.
    mysql -h welby-sz-support.ctqjagpjdliy.ap-northeast-1.rds.amazonaws.com -uwelby -p
#### 3.
    E9aNYhCH
#### 4.
    show databases;
#### 5.
    use kokorocare_db;
#### 6.
    show tables;
