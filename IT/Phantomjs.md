< PhantomJs　インストール >
========================
***

### 1. composer.jsonに追加
```
  "scripts": {
      "post-install-cmd": [
          "PhantomInstaller\\Installer::installPhantomJS"
      ],
      "post-update-cmd": [
          "PhantomInstaller\\Installer::installPhantomJS"
      ]
  }
  "config": {
        "bin-dir": "bin"
  }
```

### 2. commandで root フォルダに下記を入力  
> $ composer require "jonnyw/php-phantomjs:4.* "

### 3. Controllerで基本テスト   
※ set できない場合はControllerで下記を入力
>$client->getEngine()->setPath(ROOT . DS . 'bin' . DS . 'phantomjs.exe');





## < PhantomJs　PDF印刷例示 >

```php
<?php
use JonnyW\PhantomJs\Client;

public function printPdf()
    {
        $this->autoRender = FALSE;

        //レポートページをファイルに保存　webroot/
        $client = Client::getInstance();
        $client->getEngine()->setPath(ROOT . DS . 'bin' . DS . 'phantomjs.exe');

        $request = $client->getMessageFactory()->createPdfRequest();
        $response = $client->getMessageFactory()->createResponse();

        $request->setUrl(ADDR_REPORT_SELF . "1" . "/" . "2015-05-03");
        $request->setOutputFile(PDF_FILE);
        $request->setFormat('A4');
        //紙を縦に設定
        $request->setOrientation('portrait');
        $request->setMargin('0cm');
        $client->send($request, $response);

        //ファイルをユーザ側に保存
        $pdf_file_name = "report_"."patientnumber".".pdf";
        header ("Content-disposition: attachment; filename=" . $pdf_file_name);
        readfile(PDF_FILE);
        //webrootのファイルを削除
        unlink(PDF_FILE);
        exit();
    }
```
