10.3
cd vbox/box04/
sandbox up

ssh vagrant@192.168.10.50
pwd : vagrant

 vagrant ssh






基本Linuxコマンド
========================
***

### ルート権限
```
sudo su -
```

### ルート権限終了
```
exit
```

>「$」の場合は一般ユーザ、<br>
>「#」の場合はルートでログインしている状況


### ユーザパスワード変更
```
sudo passwd [postgres]
```


### タイムゾーン
#### タイムゾーン確認
```
date
###とか
strings /etc/localtime
```

#### 現在のタイムゾーン変更
```
### バックアップ
cp /etc/localtime /etc/localtime.org

### タイムゾーンファイルの変更
ln -sf  /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
```

#### 再起動してもタイムゾーンを有志したい場合の設定
```
### バックアップ
cp /etc/sysconfig/clock /etc/sysconfig/clock.org

### 下記のように変更
ZONE="Asia/Tokyo"
```

#### 再起動
```
reboot
```



基本Linux環境構築
========================
***

## 0. sudo yum install -y vim

## インストール jdk
```
yum search openjdk
yum install java-1.7.0-openjdk.x86_64
yum install java-1.7.0-openjdk-devel.x86_64
java -version
```

## インストール php(php5.6以上)
```
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ivh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

yum remove php*

yum --enablerepo=epel,remi info php
yum install php php-common --enablerepo=remi,remi-php56
yum install php-intl php-mysqlnd php-mbstring php-gd php-pdo php-gd php-xml php-devel php-opcache --enablerepo=remi,remi-php56
```

```
###ファイル確認
/etc/httpd/conf.d/php.conf
```

## インストール composer
```
curl -sS https://getcomposer.org/installer | php
```

```
###composesr.phar位置
/home/vagrant/composer.phar
```

## インストール tomcat
```
yum install wget
useradd -s /sbin/nologin tomcat
wget http://ftp.tsukuba.wide.ad.jp/software/apache/tomcat/tomcat-7/v7.0.70/bin/apache-tomcat-7.0.70.tar.gz
  ※上記のadrressはtomcatサイトでtar.gzファイルのlink先確認
tar -xf apache-tomcat-7.0.70.tar.gz
```
mv apache-tomcat-7.0.70 /usr/local/tomcat
### tomcat 確認
>http://192.168.10.50:8080/　接続したら確認可能  
>Manager Appでwarファイル配備できる  
>tomcat/webappsにwarファイルを入れて、startupしたら確認できる

```
sudo /usr/local/tomcat/bin/startup.sh
sudo /usr/local/tomcat/bin/shutdown.sh
```


## インストール apache
```
yum install -y httpd
```

>apache2.4インストールしたい場合

```
cd /etc/yum.repos.d
wget https://repos.fedorapeople.org/repos/jkaluza/httpd24/epel-httpd24.repo
yum install httpd24
```

>apacheインストールされたフォルダ

```
cd /opt/rh/httpd24/root/etc/httpd
```

```
sudo /etc/init.d/httpd24-httpd start
sudo /etc/init.d/httpd24-httpd stop
```

>OS を起動したときに apache も起動するように設定
```
chkconfig httpd on
```


## apacheとtomcatを連動

### 1. プログラム名で設定
```
sudo vi /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf
```

```
<Location /gitbucket/ >
  ProxyPass ajp://localhost:8009/gitbucket/>
</Location>
```

### 2. プログラム名で設定

```
sudo vi /usr/local/tomcat/conf/server.xml
コメントアウトする
<Connector port="8009" protocol="AJP/1.3" redirectPort="8443" />
```

### 3. tomcat, apache 再起動




## postgresqlインストール
### 1. 検索
```
yum search postgresql
```

### 2. インストール
```
yum install postgresql-server
```

### 3. postgres　インストール確認
```
rpm -qa | grep postgres
```

### 4. どこか確認
```
which psql
```

### 5 サービスに登録されるか確認
```
ls /etc/init.d/
```

### 6. OS を起動したときに PostgreSQL も起動するように設定
```
chkconfig postgresql on
```

### 7. 自動起動状態確認
```
chkconfig --list postgresql
```

### 8. 起動
```
service postgresql start
```
※（最初にエラーが起こる場合はsudo mkdir -p /var/lib/pgsql/data/を実行）


### 9. 外部で接続できるように設定
```
sudo vi /var/lib/pgsql/data/postgresql.conf
<br><br>
listen_addresses = 'localhost,192.168.10.20'
port = 5432
```

```
sudo vi /var/lib/pgsql/data/pg_hba.conf
host all all 0.0.0.0 0.0.0.0 trust
```

### 10. postgresql 再起動
```
service postgresql restart
```

### 11. postgresql接続
```
su - postgres
```




### MySql インストール
### 1. インストール
```
yum install mysql-server
```

### 2. utf-8設定
```
vi /etc/my.cnf

default-character-set = utf8
[mysql]
default-character-set = utf8
```

### 3. 起動
```
service mysqld start
```

### 4. OS を起動したときに mysql も起動するように設定
```
chkconfig mysqld on
```

### 5. 自動起動状態確認
```
chkconfig --list mysqld
```

### 6. 接続
```
mysql -u [ユーザ名] -p
```

### 7. 外部で接続できるように設定<br>(新ユーザを生成し、権限設定)
```
grant all privileges on [データベース名.テーブル名] to [ユーザ名@"ホスト名"] identified by ['パスをーど'] with grant option;

例示）
1. kicoユーザが192.168.10.xのIPで接続する場合はtestデータベースに接続できる
grant all privileges on [test.*] to [kico@"192.168.10.%"] identified by ['password'] with grant option;

2. kicoユーザがどこで接続しても全体のデータベースに接続できる
grant all privileges on [*.*] to [kico] identified by ['password'] with grant option;
```


## atom-remote-sync
### atom開く

### 「File」→「Settings」→「Install」

### 「」





## postgresql 実行
### ユーザ一覧
```
psql
select * from pg_shadow;
\q
```

### ユーザのパスワード変更
```
psql
alter role [postgresql_user_id] with password '[password]'
```

### データベース一覧
```
psql -l
```

### データベース生成
```
createdb mydb
```

### ユーザ生成
```
createuser [postgresql_user_id]
```






## mysql 実行

### ユーザ一覧
```
select user,host,password from mysql.user;
```

### ユーザパスワード変更
```
set password for [root@localhost]=password('パスワード');
```

### データベース一覧
```
show databases
```

### データベース使用
```
use データベース名
```



## cakePhp3プロジェクト生成
```
sudo php /home/vagrant/composer.phar create-project --prefer-dist cakephp/app project_name
```

### cssができない場合は下記の内容を```vim /etc/httpd/conf/httpd.conf```に追加
```
<Directory "/var/www/html/cakephp3">
AllowOverride All
</Directory>
```

```
service httpd restart
```
