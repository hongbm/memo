Hugo
========================
***

## Hugoとは
> HtmlファイルをTempleteとして便利に作成するプログラム

## ・ Hugoインストール(Windows)
[Hugo install windows](https://gohugo.io/tutorials/installing-on-windows/)参照


### 1. フォルダ作成
- C:\Hugo
- C:\Hugo\Sites
- C:\Hugo\bin

### 2. 実行ファイルをダウンロード
  > [Hugo release](https://github.com/spf13/hugo/releases)でWindows用ファイルをダウンロードし、
  > ```C:\Hugo\bin```に```hugo.exe```ファイルを保存

### 3. パスー設定
  > PATHに```C:\Hugo\bin```を追加

### 4. インストール確認
  > cmdで```hugo version```を入力して確認



## ・ 新しいHugoプロジェクトを生成

> 1. cmdに接続
> 1. ```cd C:\Hugo\Sites```を入力
> 1. ```hugo new site project_name```を入力




## ・ Hugoプロジェクトの構成

- archetypes
```
  hugo newコマンドを利用する時、使用される
```
- content
```
  コンテンツファイルを保存
```
- data
```
  store configuration
  YAML, JSON,or TOML format
```
- layouts
```
  テンプレートファイルを保存
```
- static
```
  css, js, static content
```
- config.toml
```
  設定ファイル
  TOML format
```














hugo server --buildDrafts
