Linux コマンド
========================
***

|コマンド  |意味                                |
|:------:|:----------------------------------:|
|clear   |画面をクリアする                       |
|ls      |ファイル、ディレクトリ情報を表示する        |
|ls -a   |全体のファイル、ディレクトリ情報を表示する  |
|cd      |カレント、ディレクトリの変更               |
|   |                 |
|   |                 |
|   |                 |
|~       |ホームディレクトリ                      |
|..      |上位ディレクトリ                      |
