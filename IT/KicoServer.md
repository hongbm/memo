
内部VPSサーバ接続方法
========================
***


## 管理サーバ
### -　Vagrant1.8.1 + VirtualBox5.0.20 + chefdk
> アドレス : 192.168.10.3    
> kico // kico@@2016

仮想BOX定義
```
/home/kico/vbox
```

<br>

## VPS1
### -　PMS、Welby-stage
> アドレス : 192.168.10.20  
> kico // kico@@2016

 - pms接続
```
ssh vps-pms
```
<br>
 - welby接続
```
ssh welby-stage-phr
```
<br>
 - ログ確認例
```
tail -f -n 100 /var/html/kokorocare-stage.welby.jp/current/logs/error.log
tail -f -n 100 /var/html/kokorocare-stage.welby.jp/current/logs/debug.log
```
```
tail -f -n 100 /var/html/kokorocare-doctor-stage.welby.jp/current/logs/error.log
tail -f -n 100 /var/html/kokorocare-doctor-stage.welby.jp/current/logs/debug.log
```


- vagrant sahara

  >- sandboxモード実行
    ```
    $ vagrant sandbox on
    ```
  >- sandboxモード終了(コミットしていない変更は削除)
    ```
    $ vagrant sandbox off
    ```
  <br>
  >- ロールバック
    ```
    $ vagrant sandbox rollback
    ```
  >- コミット
    ```
    $ vagrant sandbox commit
    ```
  <br>
  >- sandboxのステータス確認
    ```
    $ vagrant sandbox status
    ```

<br>
<br>



** ※ サーバーIP変更時はインタネットで[こちら](http://192.168.10.1/)に接続 **
> アドレス : http://192.168.10.1  
> admin // kico21


### 新しいvboxを作成後
#### 1. ssh vagrantで接続
#### 2. ip確認
```
ifconfig
### HWaddrをコピー
```

#### http://192.168.10.1/ に接続
```
「DHCP固定割当エントリで」追加
```

#### 再起動
```
sudo ifdown eth1
sudo ifup eth1
###とか
sudo service network restart
```

#### 確認
```
ifconfig
```

####  ※ もし変わらない場合
##### 1. 設定ファイル
```
sudo vim /etc/sysconfig/network-scripts/ifcfg-eth1
```
>下記の内容を追加
```
BOOTPROTO=none
IPADDR=192.168.10.50
GATEWAY=192.168.10.1
NETMASK=255.255.255.0
```




***



## Vagrantとは
- rubyスクリプト語
- VirtualBox等の仮想マシンを管理するプログラム
- 基本単位はbox

## Vagrantの使用理由
- チームで同一の環境を簡単に構築
- 環境構築を自動化
- 実行環境を開発と本番で揃える
- 複数の仮想マシンを起動する


> vagrant 実行
  ```
  vagrant up
  ```
