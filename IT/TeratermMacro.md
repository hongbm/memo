Tera Termの自動ログインのマクロ生成
========================
***


### Tera Termがインストールされている場所に移動
> C:\Program Files (x86)\teraterm

### ファイルをコピーし、名前を変更して貼り付け
> ssh2login.ttl

### ファイルをeditorプログラムで開き、下記の内容変更
> username
> hostname

### ファイルを「ttpmacro.exe」で開ければ接続できる
